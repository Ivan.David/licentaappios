//
//  MLModelRepository.swift
//  TennisCoach
//
//  Created by David Ivan on 15/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import Foundation
import Firebase

class MLModelReposiory {
    let db = FirestoreDB.shared.db
    
    func fetchAllModels(completion: @escaping (_ models:[MLModel]) -> Void) {
        db.collection("models").getDocuments() {
            querySnapshot, error in
            
            if let error = error {
                print("\(error.localizedDescription)")
            } else {
                guard let querySnapshot = querySnapshot else { return }
                
                let models = (querySnapshot.documents.compactMap( {
                    MLModel(dictionary: $0.data())
                }))
                completion(models)
            }
        }
    }
}
