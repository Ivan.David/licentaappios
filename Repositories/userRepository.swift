//
//  UsersRepository.swift
//  TennisCoach
//
//  Created by David Ivan on 15/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import Foundation
import Firebase

class UserRepository {
    let db = FirestoreDB.shared.db
    
    func createUser(_ user: User) {
        let _ = db.collection("users").addDocument(data: user.dictionary) { error in
            if let error = error {
                print("Error adding document: \(error.localizedDescription)")
            } else {
                print("User created successfully")
            }
        }
    }
    
    func getCurrentUserId() -> String? {
        let user = Auth.auth().currentUser
        if let user = user {
            // The user's ID, unique to the Firebase project.
            // Do NOT use this value to authenticate with your backend server,
            // if you have one. Use getTokenWithCompletion:completion: instead.
            let uid = user.uid
            return uid
        }
        return nil
    }
}
