//
//  ModelResultRepository.swift
//  TennisCoach
//
//  Created by David Ivan on 18/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import Foundation
import Firebase

class ModelResultRepository {
    let db = FirestoreDB.shared.db
    
    func fetchAllResultsForUser(with uid: String, completion: @escaping (_ results:[ModelResult]) -> Void) {
        let resultsQuery = db.collection("results").whereField("userId", isEqualTo: uid)
        resultsQuery.order(by: "timestamp", descending: true)
        resultsQuery.getDocuments { snapshot, error in
            if let error = error {
                print("\(error.localizedDescription)")
            } else {
                guard let snapshot = snapshot else {
                    return
                }
                
                let results = (snapshot.documents.compactMap( {
                    ModelResult(dictionary: $0.data())
                }))
                completion(results)
            }
        }
    }
    
    func createModelResult(_ modelResult: ModelResult) {
        let _ = db.collection("results").addDocument(data: modelResult.dictionary) {
            error in
            if let error = error {
                print("Error adding document: \(error.localizedDescription)")
            } else {
                print("ModelResult created successfully")
            }
        }
    }
}
