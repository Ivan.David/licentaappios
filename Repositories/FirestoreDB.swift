//
//  FirebaseDB.swift
//  TennisCoach
//
//  Created by David Ivan on 15/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import Foundation
import Firebase

class FirestoreDB {
    let db = Firestore.firestore()
    
    private init(){}
    
    static let shared = FirestoreDB()
}
