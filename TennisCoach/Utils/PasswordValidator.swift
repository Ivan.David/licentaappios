//
//  PasswordValidator.swift
//  TennisCoach
//
//  Created by David Ivan on 04/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import Foundation

class PasswordValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        do {
            if try NSRegularExpression(pattern: "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$", options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                throw ValidationError("Invalid password")
            }
        } catch {
            throw ValidationError("Invalid password")
        }
        return value
    }
}
