//
//  ValidatorFactory.swift
//  TennisCoach
//
//  Created by David Ivan on 04/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import UIKit

protocol ValidatorConvertible {
    func validated(_ value: String) throws -> String
}

enum ValidatorType {
    case email
    case password
    case username
}

struct ValidationError: Error {
    var message: String
    
    init(_ message: String) {
        self.message = message
    }
}

enum VaildatorFactory {
    static func validatorFor(type: ValidatorType) -> ValidatorConvertible {
        switch type {
        case .email: return EmailValidator()
        case .password: return PasswordValidator()
        case .username: return UsernameValidator()
        }
    }
}
