//
//  UITextField+ValidatorFactory.swift
//  TennisCoach
//
//  Created by David Ivan on 04/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import UIKit

extension UITextField {
    func validatedText(validationType: ValidatorType) throws -> String {
        let validator = VaildatorFactory.validatorFor(type: validationType)
        return try validator.validated(self.text!)
    }
}
