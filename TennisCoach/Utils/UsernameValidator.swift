//
//  UsernameValidator.swift
//  TennisCoach
//
//  Created by David Ivan on 04/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import Foundation

class UsernameValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        do {
            if try NSRegularExpression(pattern: "/^[a-z0-9_-]{3,15}$/", options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                throw ValidationError("Invalid username")
            }
        } catch {
            throw ValidationError("Invalid username")
        }
        return value
    }
}
