//
//  Validator.swift
//  TennisCoach
//
//  Created by David Ivan on 04/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import Foundation

class EmailValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        do {
            if try NSRegularExpression(pattern: "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$", options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                throw ValidationError("Invalid e-mail address")
            }
        } catch {
            throw ValidationError("Invalid e-mail address")
        }
        return value
    }
}
