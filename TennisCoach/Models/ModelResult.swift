//
//  ModelResult.swift
//  TennisCoach
//
//  Created by David Ivan on 07/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import Foundation

struct ModelResult {
    let imageBase64: String
    let confidence: Float
    var outputClass: String {
        get {
            return categoryMap[String(category)] ?? ""
        }
    }
    let category: Int
    let categoryMap: [String : String]
    let userId: String
    let modelTitle: String
    let timestamp: String
    
    init(category: Int, categoryMap: [String : String], confidence: Float, userId: String, modelTitle: String, imageBase64: String, timestamp: String) {
        self.imageBase64 = imageBase64
        self.category = category
        self.categoryMap = categoryMap
        self.confidence = confidence
        self.userId = userId
        self.modelTitle = modelTitle
        self.timestamp = timestamp
    }
}

extension ModelResult: FirebaseModel {
    var dictionary: [String : Any] {
        return [
            "category" : category,
            "categoryMap" : categoryMap,
            "confidence" : confidence,
            "userId" : userId,
            "modelTitle" : modelTitle,
            "imageBase64" : imageBase64,
            "timestamp" : timestamp,
        ]
    }
    
    init?(dictionary: [String : Any]) {
        guard let category = dictionary["category"] as? Int,
            let confidence = dictionary["confidence"] as? Double,
            let userId = dictionary["userId"] as? String,
            let modelTitle = dictionary["modelTitle"] as? String,
            let categoryMap = dictionary["categoryMap"] as? [String : String],
            let imageBase64 = dictionary["imageBase64"] as? String,
            let timestamp = dictionary["timestamp"] as? String
            else {
                assertionFailure("Invalid data from DB")
                return nil
            }
        
        self.init(category: category,
                  categoryMap: categoryMap,
                  confidence: Float(confidence),
                  userId: userId,
                  modelTitle: modelTitle,
                  imageBase64: imageBase64,
                  timestamp: timestamp)
    }
}
