//
//  FirebaseModel.swift
//  TennisCoach
//
//  Created by David Ivan on 15/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import Foundation

protocol FirebaseModel {
    init?(dictionary:[String : Any])
    
    var dictionary: [String : Any] { get }
}
