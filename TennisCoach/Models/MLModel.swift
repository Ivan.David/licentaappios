//
//  File.swift
//  TennisCoach
//
//  Created by David Ivan on 07/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import Foundation

struct MLModel {
//    let id: String
    let imageUrl: String
    let title: String
    let shortDescription: String
    let longDescription: String
    let restUrl: String
    let interpretationMap: [String : String]
    
    init(imageUrl: String, title: String, shortDescription: String, longDescription: String, restUrl: String, interpretationMap: [String : String]) {
//        self.id = id
        self.imageUrl = imageUrl
        self.title = title
        self.shortDescription = shortDescription
        self.longDescription = longDescription
        self.restUrl = restUrl
        self.interpretationMap = interpretationMap
    }
}

extension MLModel: FirebaseModel {
    var dictionary: [String : Any] {
        return [
            "imageUrl" : imageUrl,
            "title" : title,
            "shortDescription" : shortDescription,
            "longDescription" : longDescription,
            "restUrl" : restUrl,
            "interpretationMap" : interpretationMap
        ]
    }
    
    init?(dictionary: [String : Any]) {
        guard let imageUrl = dictionary["imageUrl"] as? String,
            let title = dictionary["title"] as? String,
            let shortDescription = dictionary["shortDescription"] as? String,
            let longDescription = dictionary["longDescription"] as? String,
            let restUrl = dictionary["restUrl"] as? String,
            let interpretationMap = dictionary["categoryMap"] as? [String: String]
            else {
                assertionFailure("Invalid data from DB")
                return nil
        }
        
        self.init(imageUrl: imageUrl, title: title, shortDescription: shortDescription, longDescription: longDescription, restUrl: restUrl, interpretationMap: interpretationMap)
    }
}
