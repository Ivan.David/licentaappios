//
//  User.swift
//  TennisCoach
//
//  Created by David Ivan on 15/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import Foundation

struct User {
    let username: String
    let email: String
    var uid: String = ""
    
    init(username: String, email: String) {
        self.username = username
        self.email = email
    }
}

extension User: FirebaseModel {
    var dictionary: [String : Any] {
        return [
            "username" : username,
            "email" : email,
            "uid" : uid,
        ]
    }
    
    init?(dictionary: [String : Any]) {
        guard let username = dictionary["username"] as? String,
            let email = dictionary["email"] as? String else {
                assertionFailure("Invalid data from DB")
                return nil
        }
        
        self.init(username: username, email: email)
    }
}
