//
//  LogInViewController.swift
//  TennisCoach
//
//  Created by David Ivan on 03/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import UIKit
import Firebase


class LogInViewController: UIViewController {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var logInButton: UIButton!
    
    @IBAction func backButtonTouch(_ sender: Any) {
        navigationController?.popViewController(animated: false)
    }
    
    @IBAction func loginButtonTouch(_ sender: Any) {
        do {
            let email = try emailTextField.validatedText(validationType: ValidatorType.email)
            let password = try passwordTextField.validatedText(validationType: ValidatorType.password)
            
            Auth.auth().signIn(withEmail: email, password: password) { [weak self] _, error in
                let tabBarViewController = TabBarViewController()
                let navController = UINavigationController(rootViewController: tabBarViewController)
                navController.navigationBar.isHidden = false
                self?.present(navController, animated: true, completion: nil)
                }
        } catch(let error) {
            print(error)
            self.present(Alert.errorAlert((error as! ValidationError).message), animated: true, completion: nil)
        }
    }
}
