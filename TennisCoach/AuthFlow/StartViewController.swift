//
//  ViewController.swift
//  TennisCoach
//
//  Created by David Ivan on 03/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    let authStoryboard = UIStoryboard(name: "Authentication", bundle: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func signUpButtonTouch() {
        let signUpVC = authStoryboard.instantiateViewController(withIdentifier: "SignUpViewController")
        navigationController?.pushViewController(signUpVC, animated: false)
    }
    
    @IBAction func logInButtonTouch() {
        let logInVC = authStoryboard.instantiateViewController(withIdentifier: "LogInViewController")
        navigationController?.pushViewController(logInVC, animated: false)
    }
}

