//
//  SignUpViewController.swift
//  TennisCoach
//
//  Created by David Ivan on 03/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
   
    @IBAction func onBackButtonTouch(_ sender: Any) {
        navigationController?.popViewController(animated: false)
    }
    
    @IBAction func signUpButtonTouch(_ sender: Any) {
        do {
            let email = try emailTextField.validatedText(validationType: ValidatorType.email)
            let password = try passwordTextField.validatedText(validationType: ValidatorType.password)
            guard let username = usernameTextField.text else {
                // hack
                return
            }
            
            Auth.auth().createUser(withEmail: email, password: password) { [weak self] _, error in
                if !(error != nil) {
                    guard let user = Auth.auth().currentUser else {
                        self?.present(Alert.errorAlert((error as! ValidationError).message), animated: true, completion: nil)
                        return
                    }
                    
                    self?.addUserToFirebase(uid: user.uid, username: username, email: email)
                    self?.navigateToHome()
                } else {
                    self?.present(Alert.errorAlert((error as! ValidationError).message), animated: true, completion: nil)
                }
            }
        } catch (let error) {
            print(error)
            self.present(Alert.errorAlert((error as! ValidationError).message), animated: true, completion: nil)
        }
    }
    
    private func addUserToFirebase(uid: String, username: String, email: String) {
        var user = User(username: username, email: email)
        user.uid = uid
        let userRepository = UserRepository()
        userRepository.createUser(user)
    }
    
    private func navigateToHome() {
        let tabBarViewController = TabBarViewController()
        let navController = UINavigationController(rootViewController: tabBarViewController)
        navController.navigationBar.isHidden = false
        present(navController, animated: true, completion: nil)
    }
}
