//
//  TabBarViewController.swift
//  
//
//  Created by David Ivan on 06/05/2019.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 225, height: 265)
        layout.minimumInteritemSpacing = 12
        layout.minimumLineSpacing = 12
        layout.headerReferenceSize = CGSize(width: 0, height: 20)
        layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        let modelsCollectionViewController = ModelsCollectionViewController.init(collectionViewLayout: layout)
        modelsCollectionViewController.tabBarItem = UITabBarItem(title: "Tennis coach", image: UIImage(named: "tennis-coach"), selectedImage: nil)
        
        
        let userHistoryViewController = UserHistoryViewController()
        userHistoryViewController.tabBarItem = UITabBarItem(title: "My Results", image: UIImage(named: "my-results"), selectedImage: nil)
        
        let viewControllerList = [ modelsCollectionViewController, userHistoryViewController ]
        
        viewControllers = viewControllerList
    }
    
    func switchToResultsTab() {
        self.selectedIndex = 1
    }
    
}
