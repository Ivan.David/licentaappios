//
//  ModelPresentationViewController.swift
//  TennisCoach
//
//  Created by David Ivan on 06/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import AVFoundation
import Photos
import Alamofire

enum AttachmentType: String {
    case camera, video, photoLibrary
}

class ModelPresentationViewController: UIViewController {
    
    @IBOutlet weak var modelImageView: UIImageView!
    @IBOutlet weak var modelDescriptionLabel: UILabel!
    
    var model: MLModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.navigationController?.setNavigationBarHidden(false, animated: true)

        guard let model = model,
            let url = URL(string: model.imageUrl)
            else { return }
        let placeholderImage = UIImage(named: "ball-placeholder")
        modelImageView.af_setImage(withURL: url, placeholderImage: placeholderImage)
        
        modelDescriptionLabel.text = model.longDescription
        title = model.title
    }
    
    func prepareWith(model: MLModel) {
        self.model = model
    }

    @IBAction func useExitingVideoButtonTouch(_ sender: Any) {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status{
        case .authorized:
                openVideoLibrary()
        case .denied, .restricted:
            self.present(Alert.addAlertForSettings(AttachmentType.video), animated: true, completion: nil)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ [weak self] (status) in
                self?.openVideoLibrary()
            })
        default:
            break
        }
    }
    
    private func openVideoLibrary() {
        // Display Photo Library
        let pickerController = UIImagePickerController()
        pickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        pickerController.mediaTypes = [kUTTypeMovie as String]
        pickerController.delegate = self
        
        present(pickerController, animated: true, completion: nil)
    }
    
    private func openCamera() {
        // 1 Check if project runs on a device with camera available
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let pickerController = UIImagePickerController()
            // 2 Present UIImagePickerController to take video
            pickerController.sourceType = .camera
            pickerController.mediaTypes = [kUTTypeMovie as String]
            pickerController.delegate = self
            
            present(pickerController, animated: true, completion: nil)
        } else {
            print("Camera is not available")
        }
    }
    
    @IBAction func takeNewVideoButtonTouch(_ sender: Any) {
            let status = AVCaptureDevice.authorizationStatus(for: .video)
            switch status {
            case .authorized: // The user has previously granted access to the camera.
                self.openCamera()
                
            case .notDetermined: // The user has not yet been asked for camera access.
                AVCaptureDevice.requestAccess(for: .video) { [weak self] granted in
                    if granted {
                        self?.openCamera()
                    }
                }
            //denied - The user has previously denied access.
            //restricted - The user can't grant access due to restrictions.
            case .denied, .restricted:
                self.present(Alert.addAlertForSettings(AttachmentType.video), animated: true, completion: nil)
                return
                
            default:
                break
            }
    }
}

extension ModelPresentationViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let model = model else { return }
        // To handle image
        if let _ = info[UIImagePickerControllerOriginalImage] as? UIImage {
            print("Please select a video")
            return
        }
        // To handle video

        let baseUrl = "http://127.0.0.1:5000"
        if let videoUrl = info[UIImagePickerControllerMediaURL] as? URL{
            print("videourl: ", videoUrl)
            //trying compression of video
            let data = NSData(contentsOf: videoUrl)!
            print("File size before compression: \(Double(data.length / 1048576)) mb")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                multipartFormData.append(videoUrl, withName: "file")
                
            }, to: baseUrl + model.restUrl)
            { (result) in
                switch result {
                case .success(let upload, _ , _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        
                        print("progress")
                    })
                    
                    upload.responseJSON { response in
                        let result = response.result
                        let _ = result.flatMap { [weak self] prediction in
                            guard let prediction = prediction as? Dictionary<String, Any> else { return }
                            
                            let userRepository = UserRepository()
                            guard let userId = userRepository.getCurrentUserId(),
                                let category = prediction["model_response"] as? Int,
                                let modelTitle = prediction["model_name"] as? String,
                                let categoryMap = prediction["category_map"] as? [String : String],
                                let imageBase64 = prediction["base64Image"] as? String
                                else { return }
                            
                            let timestamp = String(NSDate().timeIntervalSince1970)
                            let modelResult = ModelResult(category: category, categoryMap: categoryMap,
                                                          confidence: 0.5, userId: userId, modelTitle: modelTitle,
                                                          imageBase64: imageBase64, timestamp: timestamp)
                            let modelResultRepository = ModelResultRepository()
                            modelResultRepository.createModelResult(modelResult)
                            
                           
                            let alert = Alert.analysisCompleteAlert(with: self?.navigationController)
                            self?.navigationController?.present(alert, animated: true, completion: nil)
                            guard let tabBarController = self?.navigationController?.viewControllers.first as? TabBarViewController else {
                                return
                            }
                            tabBarController.switchToResultsTab()
                        }
                    }
                case .failure(let encodingError):
                    print("failed")
                    print(encodingError)
                    
                }
            }
        } else {
            print("Something went wrong in video")
        }
        self.dismiss(animated: true, completion: nil)
    }
}
