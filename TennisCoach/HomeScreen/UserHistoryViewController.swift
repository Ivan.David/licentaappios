//
//  UserHistoryViewController.swift
//  TennisCoach
//
//  Created by David Ivan on 06/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import UIKit

private let reuseIdentifier = "UserHistoryTableViewCell"

class UserHistoryViewController: UIViewController {
    var modelResults: [ModelResult]?

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        
        let cellNib = UINib(nibName: "UserHistoryTableViewCell", bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: reuseIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.topItem?.title = "My Results"
        loadData()
    }
    
    private func loadData() {
        let userRepository = UserRepository()
        guard let userId = userRepository.getCurrentUserId() else { return }
        
        let modelResultRepository = ModelResultRepository()
        modelResultRepository.fetchAllResultsForUser(with: userId, completion: updateData)
    }
    
    private func updateData(model: [ModelResult]) {
        modelResults = model
        tableView.reloadData()
    }
}

// MARK: - Table view data source
extension UserHistoryViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let modelResults = modelResults else { return 0 }
        return modelResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? UserHistoryTableViewCell, let modelResults = modelResults
            else { return UITableViewCell() }
        
        cell.prepareWith(model: modelResults[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}
