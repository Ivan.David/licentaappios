//
//  ModelsCollectionViewController.swift
//  TennisCoach
//
//  Created by David Ivan on 06/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import UIKit

private let reuseIdentifier = "ModelsCollectionViewCell"

class ModelsCollectionViewController: UICollectionViewController {
    var models: [MLModel]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cellNib = UINib(nibName: "ModelCollectionViewCell", bundle: nil)
        self.collectionView!.register(cellNib, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView?.backgroundColor = UIColor.white

       loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.topItem?.title = "Tennis Coach"
    }
    
    private func loadData() {
        let mlModelRepository = MLModelReposiory()
        mlModelRepository.fetchAllModels(completion: updateData)
    }
    
    private func updateData(model: [MLModel]) {
        models = model
        collectionView?.reloadData()
    }
}

// MARK: UICollectionViewDataSource
extension ModelsCollectionViewController {
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        guard models != nil else { return 0 }
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let models = models else { return 0 }
        return models.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? ModelCollectionViewCell,
            let models = models
            else { return UICollectionViewCell() }
        
        cell.prepareWith(model: models[indexPath.row])
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let models = models else { return }
        
        let modelPresentationVC = ModelPresentationViewController(nibName: "ModelPresentationViewController", bundle: nil)
        modelPresentationVC.prepareWith(model: models[indexPath.row])
        
        self.navigationController?.pushViewController(modelPresentationVC, animated: true)
    }
}
