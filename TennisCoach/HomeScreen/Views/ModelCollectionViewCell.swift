//
//  ModelCollectionViewCell.swift
//  TennisCoach
//
//  Created by David Ivan on 07/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class ModelCollectionViewCell: UICollectionViewCell {

    var model: MLModel?
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    
    func prepareWith(model: MLModel) {
        self.model = model
    }
    
    override func layoutSubviews() {
        guard let model = model else {
            assertionFailure("This view must have a model, please user prepareWith:")
            return
        }
        
        guard let url = URL(string: model.imageUrl) else { return }
        let placeholderImage = UIImage(named: "ball-placeholder")
        imageView.af_setImage(withURL: url, placeholderImage: placeholderImage)
        
        titleLabel.text = model.title
        descriptionLabel.text = model.shortDescription
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 1
    }

}
