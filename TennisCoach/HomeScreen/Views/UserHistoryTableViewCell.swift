//
//  UserHistoryTableViewCell.swift
//  TennisCoach
//
//  Created by David Ivan on 07/05/2019.
//  Copyright © 2019 David Ivan. All rights reserved.
//

import UIKit

class UserHistoryTableViewCell: UITableViewCell {

    var model: ModelResult?
    
    @IBOutlet private weak var videoFrameImageView: UIImageView!
//    @IBOutlet private weak var percentageLabel: UILabel!
    @IBOutlet private weak var predictionLabel: UILabel!
    @IBOutlet weak var modelTitleLabel: UILabel!
    
    func prepareWith(model: ModelResult) {
        self.model = model
    }
    
    override func layoutSubviews() {
        guard let model = model else {
            assertionFailure("This view must have a model, please user prepareWith:")
            return
        }
        
        if let dataDecoded = Data(base64Encoded: model.imageBase64, options: .ignoreUnknownCharacters),
            let decodedimage = UIImage(data: dataDecoded) {
            videoFrameImageView.image = decodedimage
        } else {
             let placeholderImage = UIImage(named: "ball-placeholder")
            videoFrameImageView.image = placeholderImage
        }
      
        predictionLabel.text = "Prediction: " + model.outputClass
        //percentageLabel.text = String(format: "%.1f %%", model.confidence * 100)
        modelTitleLabel.text = model.modelTitle
    }
    
    override func awakeFromNib() {
        videoFrameImageView.layer.cornerRadius = 8.0
        videoFrameImageView.clipsToBounds = true
    }
}
